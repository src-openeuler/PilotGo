%define         debug_package %{nil}

Name:           PilotGo
Version:        2.1.0
Release:        2
Summary:        PilotGo is a plugable operation platform written in go
License:        MulanPSL-2.0
URL:            https://gitee.com/openeuler/PilotGo
Source0:        https://gitee.com/openeuler/PilotGo/repository/archive/v%{version}.tar.gz
# tar -xvf Source0
# cd %{name}-v%{version}/frontend/
# run 'yarn install and yarn build' in it
# tar -czvf PilotGo-web.tar.gz ../frontend/dist/
Source1:        PilotGo-web.tar.gz
Patch0:         Modify-the-logic-of-deleting-the-machine.patch
Patch1:         Open-extension_btns-permission.patch

BuildRequires:  systemd
BuildRequires:  golang

%description
PilotGo is a plugable operation platform written in go.

%package        server
Summary:        PilotGo server
Provides:       pilotgo-server = %{version}-%{release}

%description    server
PilotGo server.

%package        agent
Summary:        PilotGo agent
Provides:       pilotgo-agent = %{version}-%{release}

%description    agent
PilotGo agent.

%prep
%setup -n %{name}-v%{version}
%patch 0 -p1
%patch 1 -p1
tar -xzvf %{SOURCE1}

%build
cp -rf frontend/dist/* src/app/server/resource
# server
pushd src/app/server
GOWORK=off GO111MODULE=on go build -mod=vendor -o PilotGo-server -tags="production" main.go
popd
# agent
pushd src/app/agent
GOWORK=off GO111MODULE=on go build -mod=vendor -o PilotGo-agent main.go
popd

%install
mkdir -p %{buildroot}/opt/PilotGo/{server/log,agent/log}
# server
install -D -m 0755 src/app/server/PilotGo-server %{buildroot}/opt/PilotGo/server
install -D -m 0644 src/config_server.yaml.templete %{buildroot}/opt/PilotGo/server/config_server.yaml
install -D -m 0644 src/user.xlsx.templete %{buildroot}/opt/PilotGo/server/user.xlsx
install -D -m 0644 scripts/service/PilotGo-server.service %{buildroot}%{_unitdir}/PilotGo-server.service
# agent
install -D -m 0755 src/app/agent/PilotGo-agent %{buildroot}/opt/PilotGo/agent
install -D -m 0644 src/config_agent.yaml.templete %{buildroot}/opt/PilotGo/agent/config_agent.yaml
install -D -m 0644 scripts/service/PilotGo-agent.service %{buildroot}%{_unitdir}/PilotGo-agent.service

%post           server
%systemd_post PilotGo-server.service

%preun          server
%systemd_preun PilotGo-server.service

%postun         server
%systemd_postun PilotGo-server.service

%post           agent
%systemd_post PilotGo-agent.service

%preun          agent
%systemd_preun PilotGo-agent.service

%postun         agent
%systemd_postun PilotGo-agent.service

%files          server
/opt/PilotGo/server/PilotGo-server
/opt/PilotGo/server/config_server.yaml
/opt/PilotGo/server/user.xlsx
/opt/PilotGo/server/log
%{_unitdir}/PilotGo-server.service

%files          agent
/opt/PilotGo/agent/PilotGo-agent
/opt/PilotGo/agent/config_agent.yaml
/opt/PilotGo/agent/log
%{_unitdir}/PilotGo-agent.service

%changelog
* Thu May 16 2024 jianxinyu <jiangxinyu@kylinos.cn> - 2.1.0-2
- Modify the logic of deleting the machine
- Open extension_btns permission

* Wed Apr 03 2024 jianxinyu <jiangxinyu@kylinos.cn> - 2.1.0-1
- Update package to version 2.1.0

* Wed Sep 13 2023 wubijie <wubijie@kylinos.cn> - 2.0.1-2
- Modify the README.md file

* Fri Sep 01 2023 jianxinyu <jiangxinyu@kylinos.cn> - 2.0.1-1
- Package init

