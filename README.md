# PilotGo

#### 介绍
PilotGo is a plugable operation platform written in go.

#### 使用说明
请在使用之前关闭防火墙,PilotGo各组件可以集群部署也可单机部署，采用部署需要在各组件中配置相应服务的真实ip地址。

#### 安装教程

1.  安装mysql、redis，并设置密码；
2.  安装PilotGo-server，并修改配置文件:
   >dnf install -y PilotGo-server

   >vim /opt/PilotGo/server/config_server.yaml

   http_server：addr为安装PilotGo-server地址；

   socket_server：addr为安装PilotGo-server地址；

   mysql：host_name为安装mysql地址；user_name为DB的登录用户；password为DB访问密码；

   redis：redis_conn为安装redis服务地址；redis_pwd为redis密码；

   启动服务
   >systemctl start PilotGo-server

   停止服务
   >ystemctl stop PilotGo-server

   服务状态
   >systemctl status PilotGo-server
3.  安装PilotGo-agent：
   >dnf install -y PilotGo-agent
   
   >vim /opt/PilotGo/agent/config_agent.yaml
   
   server：addr为安装PilotGo-server地址；
   
   启动服务
   >systemctl start PilotGo-agent

   停止服务
   >systemctl stop PilotGo-agent

   服务状态
   >systemctl status PilotGo-agent
4.  插件安装：
   [PilotGo-plugin-grafana插件安装](https://gitee.com/src-openeuler/PilotGo-plugin-grafana)  
   [PilotGo-plugin-prometheus插件安装](https://gitee.com/src-openeuler/PilotGo-plugin-prometheus)

#### 补充连接

1.  [PilotGo使用手册](https://gitee.com/openeuler/docs/tree/master/docs/zh/docs/PilotGo/使用手册.md)
2.  PilotGo[代码仓](https://gitee.com/openeuler/PilotGo)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
